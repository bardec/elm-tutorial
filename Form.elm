module Form exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import String exposing (length, toInt)
import Regex exposing (Regex, regex, contains)


main =
    Html.beginnerProgram { model = model, view = view, update = update }



-- MODEL


type alias Model =
    { name : String
    , password : String
    , passwordAgain : String
    , age : String
    , validationMessage : Maybe String
    }


model : Model
model =
    Model "" "" "" "" Nothing



-- UPDATE


type Msg
    = Name String
    | Password String
    | PasswordAgain String
    | Age String
    | Submit


update : Msg -> Model -> Model
update msg model =
    case msg of
        Name name ->
            { model | name = name }

        Password password ->
            { model | password = password }

        PasswordAgain password ->
            { model | passwordAgain = password }

        Age age ->
            { model | age = age }

        Submit ->
            { model | validationMessage = validate model }


uppercaseRegex : Regex
uppercaseRegex =
    regex "[A-Z]"


lowercaseRegex : Regex
lowercaseRegex =
    regex "[a-z]"


numericalRegex : Regex
numericalRegex =
    regex "[0-9]"


matchesRegex : String -> Bool
matchesRegex string =
    contains uppercaseRegex string
        && contains lowercaseRegex string
        && contains numericalRegex string


ageIsNumber : String -> Bool
ageIsNumber maybeAge =
    case (toInt maybeAge) of
        Ok age ->
            if (age >= 0) then
                True
            else
                False

        Err _ ->
            False


validate : Model -> Maybe String
validate model =
    if model.password /= model.passwordAgain then
        Just "Passwords do not match!"
    else if (length model.password) <= 8 then
        Just "Password much be greater than 8 characters"
    else if not (matchesRegex model.password) then
        Just "Password must contain uppercase, lowercase, and\n                numbers"
    else if not (ageIsNumber model.age) then
        Just "Age must be a positive integer"
    else
        Nothing



-- VIEW


view : Model -> Html Msg
view model =
    div []
        [ input [ type_ "text", placeholder "Name", onInput Name ] []
        , input [ type_ "age", placeholder "Age", onInput Age ] []
        , input [ type_ "password", placeholder "Password", onInput Password ] []
        , input [ type_ "password", placeholder "Re-enter Password", onInput PasswordAgain ] []
        , button [ onClick Submit ] [ text "Submit" ]
        , viewValidation model
        ]


viewValidation : Model -> Html msg
viewValidation model =
    let
        ( color, message ) =
            case model.validationMessage of
                Just message ->
                    ( "red", message )

                Nothing ->
                    ( "green", "OK" )
    in
        div [ style [ ( "color", color ) ] ] [ text message ]
