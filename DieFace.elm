module DieFace exposing (..)

import Html exposing (Html, button, h1, div, text, img)
import Html.Attributes exposing (src)
import Html.Events exposing (onClick)
import Svg exposing (circle, rect, svg)
import Svg.Attributes exposing (..)
import Random


main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- Model


type alias Model =
    { firstDieFace : Int
    , secondDieFace : Int
    }



-- Update


type Msg
    = Roll
    | NewFace ( Int, Int )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Roll ->
            ( model, Random.generate NewFace (Random.pair (Random.int 1 6) (Random.int 1 6)) )

        NewFace ( newFirstFace, newSecondFace ) ->
            ( Model newFirstFace newSecondFace, Cmd.none )



-- View


view : Model -> Html Msg
view model =
    div []
        [ dieImage model.firstDieFace
        , dieImage model.secondDieFace
        , button [ onClick Roll ] [ text "Roll" ]
        ]


dieImage : Int -> Html Msg
dieImage number =
    let
        middleDot =
            circle [ cx "60", cy "60", r "8", fill "black" ] []

        topLeftDot =
            circle [ cx "30", cy "30", r "8", fill "black" ] []

        middleLeftDot =
            circle [ cx "30", cy "60", r "8", fill "black" ] []

        bottomLeftDot =
            circle [ cx "30", cy "90", r "8", fill "black" ] []

        topRightDot =
            circle [ cx "90", cy "30", r "8", fill "black" ] []

        middleRightDot =
            circle [ cx "90", cy "60", r "8", fill "black" ] []

        bottomRightDot =
            circle [ cx "90", cy "90", r "8", fill "black" ] []

        element =
            case number of
                1 ->
                    [ middleDot ]

                2 ->
                    [ topRightDot, bottomLeftDot ]

                3 ->
                    [ topRightDot, middleDot, bottomLeftDot ]

                4 ->
                    [ topRightDot, bottomRightDot, topLeftDot, bottomLeftDot ]

                5 ->
                    [ topRightDot
                    , bottomRightDot
                    , topLeftDot
                    , bottomLeftDot
                    , middleDot
                    ]

                6 ->
                    [ topRightDot
                    , bottomRightDot
                    , topLeftDot
                    , bottomLeftDot
                    , middleLeftDot
                    , middleRightDot
                    ]

                _ ->
                    []
    in
        svg []
            ((rect
                [ x "10"
                , y "10"
                , width "100"
                , height "100"
                , stroke "black"
                , fill "white"
                ]
                []
             )
                :: element
            )



-- Subscriptions


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


init : ( Model, Cmd Msg )
init =
    ( Model 1 1, Cmd.none )
